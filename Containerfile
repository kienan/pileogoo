FROM scratch
ADD root.tar.gz /
Add boot.tar.gz /boot
USER root

# # Import our image
# COPY rpi.img /root/rpi.img
# # Mount image
# # @see https://www.boulderes.com/resource-library/building-raspberry-pi-disk-images-with-docker-a-case-study-in-software-automation
# RUN losetup -Pf /root/rpi.img
# RUN export RPILOOP=$(losetup -j /root/rpi.img | cut -d ':' -f 1)
# RUN mount "/dev/${RPILOOP}p2" /mnt
# RUN mount "/dev/${RPILOOP}p1" /mnt/boot
# RUN mount -o bind /dev /mnt/dev
# RUN mount -o bind /proc /mnt/proc
# RUN mount -o bind /sys /mnt/sys
# RUN chroot /mnt
# Do stuff inside the chroot
RUN hostname -f
RUN cat /etc/shadow | grep pi
# Configure dwc2
RUN echo 'dwc2' >> /etc/modules
RUN echo 'dtoverlay=dwc2' >> /boot/config.txt
RUN sed -i 's/rootwait/rootwait modules-load=dwc2/' /boot/cmdline.txt
RUN echo 'g_mass_storage' >> /etc/modules
RUN echo 'options g_mass_storage file=/piusb.bin stall=0 ro=1' > /etc/modprobe.d/piusb.conf
RUN dd if=/dev/zero of=/piusb.bin bs=1M count=2048
RUN mkdosfs /piusb.bin -F 32 -I
RUN mkdir /mnt/usb_share
RUN echo '/piusb.bin /mnt/usb_share vfat users,gid=mariner,umask=002 0 2' >> /etc/fstab

# Configure serial port comm to control the printer
RUN echo 'enable_uart=1' >> /boot/config.txt
# RUN systemctl stop serial-getty@ttyS0
RUN systemctl disable serial-getty@ttyS0
RUN sed -i 's/console=serial0,115200//' /boot/cmdline.txt

# Install mariner
RUN curl -sL gpg.l9o.dev | sudo apt-key add -
RUN echo "deb https://ppa.l9o.dev/raspbian ./" | sudo tee /etc/apt/sources.list.d/l9o.list
RUN apt-get update
RUN apt-get install -y mariner3d

# Quality of Life in the terminal
RUN apt-get install -y tmux

# Convert keymap to us
RUN sed -i 's/gb/us/' /etc/default/keyboard

# Set up wifi by getting raspberry pi os to do the shim work
# @see https://core-electronics.com.au/tutorials/raspberry-pi-zerow-headless-wifi-setup.html
RUN if [ -n "$RPI_WIFI_NETWORK" ] ; then \
    wpa_passphrase "$RPI_WIFI_NETWORK" "$RPI_WIFI_PASSPHRASE" >> /etc/wpa_supplicant.conf  ; \
    fi

# Enable SSH access
RUN systemctl enable ssh

# Remove the resize on boot
RUN sed -i 's#/usr/lib/raspi-config/init_resize.sh#/sbin/init#' /boot/cmdline.txt
RUN sed -i 's/fsck.repair=yes//' /boot/cmdline.txt
RUN update-rc.d resie2fs_once remove && rm /etc/init.d/resize2fs_once

# Change pi user's password
RUN if [ -n "$RPI_PASSWORD" ] ; then echo "pi:${RPI_PASSWORD}" | chpasswd ; fi

# Clean-up
RUN apt-get clean

# Create output files
#RUN tar -C / -czf /root/root.tar.gz --exclude='/boot/*' --exclude='/sys/*' \
#  --exclude='/proc/*' --exclude='/run/*' --exclude='/dev/*' \
#  --exclude=/root/root.tar.gz /
#RUN tar -C /boot -czf /root/boot.tar.gz
# # Leave the chroot and cleanup
# RUN exit
# RUN umount /mnt/dev
# RUN umount /mnt/proc
# RUN umount /mnt/sys
# RUN sync
# RUN umount /mnt/boot
# RUN unmount /mnt
# RUN losetup -d /dev/"${RPILOOP}"