#!/bin/bash +ex

dd if=/dev/zero of=build/rpi.img bs=1M count=4096
# Copy the first 8k from the original rpi.img, which seems to have
# non-zeroed content. Without this, the Pi doesn't seem to boot.
# It could be that there is the binary boot blob for the GPU
# in that section?
dd if=rpi.img of=build/rpi.img bs=8192 count=1 conv=notrunc
sudo losetup -Pf build/rpi.img
# Partition:
# start: 8192, size 256M typ  W95 FAT32
DEVICE=$(losetup -j build/rpi.img | cut -d ':' -f 1)
parted --script "${DEVICE}" \
       rm 2 \
       mkpart primary ext4 272629760B 4022337536B
mkfs.vfat "${DEVICE}p1"
mkfs.ext4 "${DEVICE}p2"
TMP=$(mktemp -d)
mount "${DEVICE}p1" "$TMP"
tar -C "$TMP" -xf build/all.tar --strip=1 boot/
umount "$TMP"
mount "${DEVICE}p2" "$TMP"
tar -C "$TMP" -xf build/all.tar --exclude='boot/*'
umount "$TMP"
losetup -D "${DEVICE}"
rm -rf "$TMP"
