#!/bin/bash

TMPDIR=$(mktemp -d)
losetup -Pf rpi.img
LODEVICE=$(losetup -j rpi.img | cut -d ':' -f 1)
mount "${LODEVICE}p2" "${TMPDIR}"
tar -C "${TMPDIR}" -czf root.tar.gz .
mount "${LODEVICE}p1" "${TMPDIR}/boot"
tar -C "${TMPDIR}/boot" -czf boot.tar.gz .
umount "${TMPDIR}/boot"
umount "${TMPDIR}"
rm -rf "${TMPDIR}"
losetup -D "${LODEVICE}"
