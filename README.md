# Pileogoo

An image for having a Raspberry Pi control an Elegoo saturn.

This is based off of https://l9o.dev/posts/controlling-an-elegoo-mars-pro-remotely/

This particular image is principally aimed at the Raspberry Pi (512M original Rev) with a wifi dongle.

## Building

1. Download the raspberry image and link rpi.img to that file
2. Run the following commands

```
# Creates root.tar.gz and boot.tar.gz
sudo ./scripts/extract_root.sh

# Export any settings (optional)
export RPI_WIFI_NETWORK=mywifi
export RPI_WIFI_PASSPHRASE=mywifi
export RPI_PASSWORD=raspberry

# Run the build
podman build --rm=false --platform linux/armv/v7 -t localhost/pileogoo .
CONTAINERID=$(podman create localhost/pileogoo /bin/bash)
podman export -o build/all.tar "${CONTAINERID}"

# Creates a new image from the exported copy
sudo ./scripts/extract_build.sh
rm ./build/all.tar
```

### Errors

standard_init_linux.go:228: exec user process caused: exec format error

 * this is caused by the image not matching the architecture: make sure you
 can run non-native archiectures by checking the Installation steps.

## Installation

Install `qemu-system-arm`, `qemu-user-static`, and `qemu-user-binfmt` to provide
arm architectures.

Note that not all versions of the Raspberry Pi are supported in qemu. For eg.

```
$ qemu-system-arm -M help | grep rasp
raspi0               Raspberry Pi Zero (revision 1.2)
raspi1ap             Raspberry Pi A+ (revision 1.1)
raspi2               Raspberry Pi 2B (revision 1.1) (alias of raspi2b)
raspi2b              Raspberry Pi 2B (revision 1.1)
```
